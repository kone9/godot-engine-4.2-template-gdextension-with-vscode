#include "CuboExample.h"
#include <godot_cpp/classes/engine.hpp>

#include <godot_cpp/classes/area3d.hpp>
#include <godot_cpp/classes/rigid_body3d.hpp>

#include <godot_cpp/classes/scene_tree.hpp>

#include <godot_cpp/variant/utility_functions.hpp>

#include <godot_cpp/classes/timer.hpp>

using namespace godot;


CuboExample::CuboExample()
{
    if(Engine::get_singleton()->is_editor_hint()) return;
	
}

CuboExample::~CuboExample()
{

}

void CuboExample::_bind_methods()
{
    //property editor
    ClassDB::bind_method(D_METHOD("get_velocity_rotation"), &CuboExample::get_velocity_rotation);
    ClassDB::bind_method(D_METHOD("set_velocity_rotation", "velocityRotation"), &CuboExample::set_velocity_rotation);
    ClassDB::add_property("CuboExample", PropertyInfo(Variant::FLOAT, "velocityRotation"), "set_velocity_rotation", "get_velocity_rotation");

    //senial area enter
    ClassDB::bind_method(D_METHOD("_on_area_3d_area_entered"), &CuboExample::_on_area_3d_area_entered);

    //senial timer
    ClassDB::bind_method(D_METHOD("_on_timer_delay_init_timeout"),&CuboExample::_on_timer_delay_init_timeout);


}

void CuboExample::_ready()
{
    if(Engine::get_singleton()->is_editor_hint()) return;
    //UtilityFunctions::print("hola GDextension 3D");

    UtilityFunctions::print("hola");


    if( get_node<Timer>("TimerDelayInit") == nullptr) return;
    get_node<Timer>("TimerDelayInit")->start();

}

void CuboExample::_process(double delta)
{
    if(Engine::get_singleton()->is_editor_hint()) return;
    rotate_y(velocityRotation * delta);
}

void CuboExample::set_velocity_rotation(const float velocity) {
	velocityRotation = velocity;
}

float CuboExample::get_velocity_rotation() const {
	return velocityRotation;
}


void CuboExample::_on_area_3d_area_entered(Area3D* area)
{
    //UtilityFunctions::print("hola mundo");
}


void CuboExample::_on_timer_delay_init_timeout()
{
    UtilityFunctions::print("hola delay");
}